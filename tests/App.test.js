import { calculateTimes } from "../src/Methods"

describe('Pruebas en el App principal', () => { 
  test('calculateTimes should return an object with the time to xmas',()=>{
        let o = calculateTimes();
        expect(o.days).toBeLessThan(365);
        expect(o.days).toBeLessThan(24);
  })
 })