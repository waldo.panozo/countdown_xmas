describe('Pruebas en <DemoComponent/>>', () => {

    test('Prueba no debe fallar',()=>{
        
        ///iniicializacion
    
        const message1 = 'Hola Mundo';
        //2 estimulo
    
        const message2 = message1.trim();
    
    
        //3 asserciones , observar comportamiento
    
        expect(message1).toBe(message2)
    
    })
})
