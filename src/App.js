import React, { useEffect, useState } from 'react'
import Numero from './Components/Numero';
import './App.css'
import { FcAlarmClock } from "react-icons/fc";
import { BsTreeFill } from "react-icons/bs";
import { calculateTimes } from './Methods';





const App = () => {

  const [days, setDays] = useState('0');
  const [hours, setHours] = useState('0');
  const [minutes, setMinutes] = useState('0');
  const [seconds, setSeconds] = useState('0');

  useEffect(() => {
    setInterval(countDown, 1000);
  }, []);


  
  const countDown = () =>{
    let resp = calculateTimes();
    setDays(resp.days);
    setHours(resp.hours);
    setMinutes(resp.minutes);
    setSeconds(resp.seconds);
    //console.log(total);
  }

  return (
    <div className='row  ' >
      <div className='col-12'>
       <h1><FcAlarmClock size={70} /> Waiting 4 X-mas <BsTreeFill /></h1>
        <div  className='col-6 inline'>

        <Numero numb={days} txt={"Days"} />
        <Numero numb={hours} txt={"Hours"} />
        <Numero numb={minutes} txt={"Minutes"} />
        <Numero numb={seconds} txt={"Seconds"} />
        </div>
      </div>
    </div>
  )
}

export default App