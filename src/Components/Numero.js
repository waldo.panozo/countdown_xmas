import React from 'react'

const Numero = ({numb,txt}) => {
  return (
    <div className='col-3 card mycard '>
        <h1>{numb}</h1>
        <div className='text'>{txt}</div>
    </div>
  )
}

export default Numero