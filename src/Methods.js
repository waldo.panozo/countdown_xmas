export function calculateTimes () {
    let now = new Date().getTime() / 1000;
    let xmas = new Date(new Date().getFullYear(), 12, 25).getTime() / 1000;
    let total = (xmas - now);
    let m =  Math.floor (total/60) ;
    let mr =  Math.floor(total%60) ;
    let h = Math.floor (m/60);
    let hr =  Math.floor(m%60);
    let d = Math.floor (h/24);
    let dr = Math.floor (h%24);
  
    return {
      days: d,
      hours: dr,
      minutes: hr,
      seconds: mr,
    }
    
  }